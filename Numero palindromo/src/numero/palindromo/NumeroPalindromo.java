
package numero.palindromo;

import java.util.Scanner;


public class NumeroPalindromo {

    
    public static void main(String[] args) {
        
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        int numero; 
        System.out.println("Ingrese un numero:");
        numero=sc.nextInt();
        
        System.out.println("El numero es Palindromo:"+ numero+ " es?:"
                
        + Palindromo(numero));
        
    }
    
    public static boolean Palindromo(int numero){
    String palabra = String.valueOf(numero);
    
    // se desplaza de izquierda a derecha
    for (int i = 0, j = palabra.length() -1; i <= j; i++, --j){
     if (palabra.charAt(i) != palabra.charAt(j)) {
     return false;
     }
    }
    return true;
    }
    
}
